# README #

## Informações importantes ##
[Endereço TeamDrive](https://drive.google.com/drive/folders/0B9wFyVrp37GtaUFoYmxaZG1PVTA)

* Cada pasta dentro deste repositório é responsável por um desafio conforme os nomes.
* Seguir a ordem das pastas conforme abaixo para realização de commits
* Cada time terá 1 hora e 30 minutos para solucionar todos os desafios

### Desafio 01 ###
* Responsável  
		- Dhiego / Moisés
* Descrição  
		- Criação de um algoritmo que busca o Enésimo número de acordo com um padrão criado.

### Desafio 02 ###
* Responsável  
		- Thiago / Paulo
* Descrição  
		- Criação de um algoritmo que deverá ser solucionado no quadro.  
		- Obs: Nesta pasta o algoritmo pode estar no formato .txt

### Desafio 03 ###
* Responsável  
		- Raphael / Maurício
* Descrição  
		- Converter um número romado para base64

### Desafio 04 ###
* Responsável  
		- Leandro / Jenyffer
* Descrição   
		- Criação de um algoritmo para descriptografia de um texto. Sugestão de saída: 4P4GU3 4 LU2

### Desafio 05 ###
* Responsável  
		- Thiago
* Descrição  
		- Criação de um algoritmo para resolver algum problema matemático.  
		- Sugestão:  
		- f(x)! = f(x) * f(x-1)!  
		- f(0)! = f(0)  
		- f(x) = round (1³/x)  


#Narrativa Sala#
Você acabou de entrar no universo do serial killer Jigsaw. Muito cuidado o portal está repleto de armadilhas e você precisa tomar muito cuidado. Pense bem, sempre existe alguma forma de escapar. Nesse portal você precisará resgatar a Joia da Alma que dá a seu portador o poder de roubar, manipular e alterar almas, seja dos vivos ou mesmo dos mortos.
Ao entrar no portal você ficou desacordado por algumas horas, Jigsaw o capturou e o colocou em um de seus jogos onde você terá que escolher entre a vida e a morte. Concentre-se para escapar e recuperar a Joia da Alma. Não se esqueça, a sobrevivência da terra depende de você.