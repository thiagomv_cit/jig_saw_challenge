package com.cit.escapetech;

import java.util.ArrayList;
import java.util.List;

public class Main {

	// n ésimo número da sequência Saw a ser identificado
	private static final int NESIMO = 68;

	public static void main(final String[] args) {
		System.out.println("Saw Sequence:   " + getSawNumbers(NESIMO));
		System.out.println("Prime Sequence: " + getPrimeNumbers(NESIMO));
	}

	private static List<Integer> getSawNumbers(final int quantity) {
		final List<Integer> numbers = new ArrayList<Integer>();
		final List<Integer> primeNumbers = getPrimeNumbers(NESIMO);
		int numberSequence = 0;
		// Sequência Saw inicia no número inteiro 0
		int sawNumber = 0;
		numbers.add(sawNumber);
		while (numbers.size() < quantity) {
			sawNumber += primeNumbers.get(numberSequence);
			numbers.add(sawNumber);
			numberSequence++;
		}
		return numbers;
	}

	private static List<Integer> getPrimeNumbers(final int quantity) {
		final List<Integer> numbers = new ArrayList<Integer>();
		// Sequência de primos inicia no número inteiro 2
		int numberSequence = 2;
		while (numbers.size() < quantity) {
			if (isPrimeNumber(numberSequence)) {
				numbers.add(numberSequence);
			}
			numberSequence++;
		}
		return numbers;
	}

	private static Boolean isPrimeNumber(final int number) {
		if (number < 5 || number % 2 == 0 || number % 3 == 0) {
			return number == 2 || number == 3;
		}

		final int maxP = (int) (Math.sqrt(number) + 2);
		for (int p = 5; p < maxP; p += 6) {
			if (number % p == 0 || number % (p + 2) == 0) {
				return false;
			}
		}

		return true;
	}
}
